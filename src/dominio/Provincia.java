package dominio;
import aplicacion.*;
import java.util.ArrayList;

public class Provincia{
    private String nombre;
    private int numeroHabitantes;

    public ArrayList<Municipio> coleccionMunicipios = new ArrayList<>();


    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public int calcularNumeroHabitantes(){
        for(int i = 0; i<coleccionMunicipios.size();i++){
            numeroHabitantes+=coleccionMunicipios.get(i).calcularNumeroHabitantes();
        }
        numeroHabitantes /=2;
           return  numeroHabitantes;

    }
    public String toString() {
        return "La provincia " + nombre + " tiene: " + numeroHabitantes +" habitantes.";
    }
}
