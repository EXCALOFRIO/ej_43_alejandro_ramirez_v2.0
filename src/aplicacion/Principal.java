package aplicacion;
import dominio.*;

public class Principal{
    public static void main (String[] args){

        Localidad sanchinarro = new Localidad();
        sanchinarro.setNumeroDeHabitantes(400);
        Localidad valdebebas = new Localidad();
        valdebebas.setNumeroDeHabitantes(300);
        Localidad barajas = new Localidad();
        barajas.setNumeroDeHabitantes(300);

        Localidad alamillos = new Localidad();
        alamillos.setNumeroDeHabitantes(200);
        Localidad tempranales = new Localidad();
        tempranales.setNumeroDeHabitantes(100);
        Localidad muntrillo = new Localidad();
        muntrillo.setNumeroDeHabitantes(100);

        Municipio madrid = new Municipio();
        madrid.setNombre ("Madrid ");

        Municipio alcorcon = new Municipio();
        alcorcon.setNombre ("Alcorón ");



        madrid.coleccionLocalidades.add(sanchinarro);
        madrid.coleccionLocalidades.add(valdebebas);
        madrid.coleccionLocalidades.add(barajas);

        madrid.calcularNumeroHabitantes();
        System.out.println(madrid);


        alcorcon.coleccionLocalidades.add(alamillos);
        alcorcon.coleccionLocalidades.add(tempranales);
        alcorcon.coleccionLocalidades.add(muntrillo);


        alcorcon.calcularNumeroHabitantes();
        System.out.println(alcorcon);

        Provincia commadrid = new Provincia();
        commadrid.setNombre ("Comunidad de Madrid ");



        commadrid.coleccionMunicipios.add(madrid);
        commadrid.coleccionMunicipios.add(alcorcon);

        commadrid.calcularNumeroHabitantes();
        System.out.println(commadrid);

    }
}
